package com.aswdc.matrimony.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.adapter.CityAdapter;
import com.aswdc.matrimony.adapter.LanguageAdapter;
import com.aswdc.matrimony.database.TblMstCity;
import com.aswdc.matrimony.database.TblMstLanguage;
import com.aswdc.matrimony.database.TblUser;
import com.aswdc.matrimony.model.CityModel;
import com.aswdc.matrimony.model.LanguageModel;
import com.aswdc.matrimony.model.UserModel;
import com.aswdc.matrimony.util.Constant;
import com.aswdc.matrimony.util.Utils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserActivity extends BaseActivity {

    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etFatherName)
    TextInputEditText etFatherName;
    @BindView(R.id.etSurName)
    TextInputEditText etSurName;
    @BindView(R.id.rbMale)
    MaterialRadioButton rbMale;
    @BindView(R.id.rbFemale)
    MaterialRadioButton rbFemale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDOB)
    TextInputEditText etDOB;
    @BindView(R.id.etPhoneNumber)
    TextInputEditText etPhoneNumber;
    @BindView(R.id.etEmailAddress)
    TextInputEditText etEmailAddress;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.chbCricket)
    CheckBox chbCricket;
    @BindView(R.id.chbFootball)
    CheckBox chbFootball;
    @BindView(R.id.chbHockey)
    CheckBox chbHockey;

    String startingDate = "1990-08-10";

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();

    UserModel userModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_add_user), true);
        setDataToView();
        setSpinnerAdapter();
        getDataForUpdate();
    }

    void getDataForUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.lbl_edit_user);
            etName.setText(userModel.getName());
            etFatherName.setText(userModel.getFatherName());
            etSurName.setText(userModel.getSurName());

            etPhoneNumber.setText(userModel.getPhoneNumber());
            etDOB.setText(userModel.getDob());
            if (userModel.getGender() == Constant.MALE) {
                rbMale.setChecked(true);
            } else {
                rbFemale.setChecked(true);
            }
            etEmailAddress.setText(userModel.getEmail());
            spCity.setSelection(getSelectedPositionFromCityId(userModel.getCityID()));
            spLanguage.setSelection(getSelectedPositionFromLanguageId(userModel.getLanguageID()));

            //set data for hobbies
            if (userModel.getHobbies().contains(chbCricket.getText().toString())) {
                chbCricket.setChecked(true);
            }
            if (userModel.getHobbies().contains(chbFootball.getText().toString())) {
                chbFootball.setChecked(true);
            }
            if (userModel.getHobbies().contains(chbHockey.getText().toString())) {
                chbHockey.setChecked(true);
            }
        }
    }

    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityID() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageId(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (languageList.get(i).getLanguageID() == languageId) {
                return i;
            }
        }
        return 0;
    }

    void setSpinnerAdapter() {
        cityList.clear();
        cityList.addAll(new TblMstCity(this).getCityList());
        languageList.clear();
        languageList.addAll(new TblMstLanguage(this).getLanguages());

        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);
        spLanguage.setAdapter(languageAdapter);

    }

    void setDataToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etDOB.setText(
                String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/ " +
                        String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/ " +
                        String.format("%02d", newCalendar.get(Calendar.YEAR)));
        setSpinnerAdapter();
    }

    @OnClick(R.id.etDOB)
    public void onEtDobClicked() {
        final Calendar newCalender = Calendar.getInstance();
        Date date = Utils.getDateFromString(startingDate);
        newCalender.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AddUserActivity.this, (datePicker, year, month, day) -> {
            etDOB.setText(String.format("%02d", day) + "/ " + String.format("%02d", (month + 1)) + "/" + year);
        },
                newCalender.get(Calendar.YEAR),
                newCalender.get(Calendar.MONTH),
                newCalender.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    String getHobbies() {
        String hobbiesList = "";
        if (chbCricket.isChecked()) {
            hobbiesList += "," + chbCricket.getText().toString();
        }
        if (chbHockey.isChecked()) {
            hobbiesList += "," + chbHockey.getText().toString();
        }
        if (chbFootball.isChecked()) {
            hobbiesList += "," + chbFootball.getText().toString();
        }
        return hobbiesList.substring(1);
    }

    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {
        if (isValidUser()) {
            if (userModel == null) {
                long lastInsertedID = new TblUser(getApplicationContext()).insertUser(etName.getText().toString(),
                        etFatherName.getText().toString(), etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        getHobbies(), Utils.getFormatedDateToInsert(etDOB.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        etEmailAddress.getText().toString(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityID(), 0);
                showToast(lastInsertedID > 0 ? "User Inserted Successfully" : "Something went wrong");
            } else {
                long lastInsertedID = new TblUser(getApplicationContext()).updateUserByID(etName.getText().toString(),
                        etFatherName.getText().toString(), etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        getHobbies(), Utils.getFormatedDateToInsert(etDOB.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        etEmailAddress.getText().toString(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityID(), userModel.getIsFavorite(), userModel.getUserId());
                showToast(lastInsertedID > 0 ? "User Updated Successfully" : "Something went wrong");
            }
            Intent intent = new Intent(AddUserActivity.this, ActivityUserListByGender.class);
            startActivity(intent);
            finish();
        }
    }

    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            isValid = false;
            etName.setError(getString(R.string.lbl_name));
        }
        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isValid = false;
            etFatherName.setError(getString(R.string.error_father_name));
        }

        if (TextUtils.isEmpty(etSurName.getText().toString())) {
            isValid = false;
            etSurName.setError(getString(R.string.error_enter_surname));
        }

        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_enter_phone));
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_valid_phone));
        }

        if (TextUtils.isEmpty(etEmailAddress.getText().toString())) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_ente_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.getText().toString()).matches()) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_valid_email));
        }

        if (spCity.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_city_select));
        }

        if (spLanguage.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_language_select));
        }

        if (!chbCricket.isChecked()) {
            if (!chbFootball.isChecked()) {
                if (!chbHockey.isChecked()) {
                    showToast(getString(R.string.error_select_one_hobby));
                    isValid = false;
                }
            }
        }
        return isValid;
    }

}
