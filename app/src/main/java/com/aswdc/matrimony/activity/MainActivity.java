package com.aswdc.matrimony.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.aswdc.matrimony.R;
import com.aswdc.matrimony.database.MyDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cvActRegistration)
    CardView cvActRegistration;
    @BindView(R.id.cvActList)
    CardView cvActList;
    @BindView(R.id.cvActFavorite)
    CardView cvActFavotire;
    @BindView(R.id.cvActSearch)
    CardView cvActSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_dashboard), false);
        new MyDatabase(this).getWritableDatabase();
    }


    @OnClick(R.id.cvActRegistration)
    public void onCvActRegistrationClicked() {
        Intent intent = new Intent(this, AddUserActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActList)
    public void onCvActListClicked() {
        Intent intent = new Intent(this, ActivityUserListByGender.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActFavorite)
    public void onCvActFavoriteClicked() {
        Intent intent = new Intent(this, UserListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cvActSearch)
    public void onCvActSearchClicked() {
        Intent intent = new Intent(this, SearchUserActivity.class);
        startActivity(intent);
    }
}
